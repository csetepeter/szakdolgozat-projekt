<?php

namespace Deployer;

use Deployer\Host\Localhost;
use Deployer\Task\Context;
use function Deployer\Support\array_to_string;

require 'recipe/symfony4.php';

function runCustom($command, $options = [])
{
    $host = Context::get()->getHost();
    $hostname = $host->getHostname();

    $workingPath = get('working_path', '');

    if (!empty($workingPath)) {
        $command = "cd $workingPath && ($command)";
    }

    $env = get('env', []) + ($options['env'] ?? []);
    if (!empty($env)) {
        $env = array_to_string($env);
        $command = "export $env; $command";
    }

    if ($host instanceof Localhost) {
        $process = Deployer::get()->processRunner;
        $output = $process->run($hostname, $command, $options);
    } else {
        $client = Deployer::get()->sshClient;
        $output = $client->run($host, $command, $options);
    }

    return rtrim($output);
}

set('ssh_type', 'native');
set('ssh_multiplexing', true);

set('application', 'szakdolgozat');

set('shared_dirs', ['website-skeleton/public/uploads', 'website-skeleton/public/media', 'website-skeleton/var/logs']);
set('shared_files', ['website-skeleton/.env.local.php', 'website-skeleton/.env.local']);
set('writable_dirs', ['website-skeleton/public/uploads', 'website-skeleton/public/media', 'website-skeleton/var/logs', 'website-skeleton/var/cache']);

set('env', function () {
    return [
        'APP_ENV' => 'test',
    ];
});

set('default_stage', 'test');

host('csetepeter.hu')
    ->stage('test')
    ->user('docker')
    ->port(22)
    ->configFile('/home/www-data/.ssh/config')
    ->identityFile('/home/www-data/.ssh/id_rsa')
    ->set('deploy_path', '/home/docker/docker-compose-dirs/szakdolgozat')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->addSshOption('ControlPath','/home/www-data/.ssh/multiplexing.data')
    ->addSshOption('UserKnownHostsFile', '/home/www-data/.ssh/known_hosts');


desc('Start the project');
task('deploy:project_start', function () {
    run('cd {{release_path}} && docker-compose -p szakdolgozat-project -f docker-compose-demo.yml down -v && docker-compose -p szakdolgozat-project -f docker-compose-demo.yml up -d && docker-compose -p szakdolgozat-project -f docker-compose-demo.yml exec -T composer composer install');
});

after('deploy:failed', 'deploy:unlock');

task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:clear_paths',
    'deploy:project_start',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup'
])->desc('Deploy');

after('deploy', 'success');
