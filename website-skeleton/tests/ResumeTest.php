<?php

namespace App\Tests;

use App\Controller\ResumeController;
use PHPUnit\Framework\TestCase;

class ResumeTest extends TestCase
{
    public function testEmail(){
        $controller = new ResumeController();
        $actual = $controller->getEmail();
        $this->assertEquals('csete.peter96@gmail.com',$actual);
    }
}
