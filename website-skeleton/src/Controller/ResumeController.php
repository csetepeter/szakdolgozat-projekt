<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ResumeController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function resume()
    {
        return $this->render('srt-resume.html.twig', ['email' => $this->getEmail()]);
    }

    public function getEmail(): String
    {
        return 'csete.peter96@gmail.com';
    }
}
