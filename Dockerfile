FROM php:7.4.9-fpm-alpine

ADD https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions /usr/local/bin/

RUN apk update \
    && apk add tzdata && cp /usr/share/zoneinfo/Europe/Budapest /etc/localtime && echo "Europe/Budapest" >  /etc/timezone && apk del tzdata \
    && chmod uga+x /usr/local/bin/install-php-extensions \
    && sync \
    && install-php-extensions xdebug opcache pdo_mysql intl gd xsl zip \
